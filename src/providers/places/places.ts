import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";

/*
  Generated class for the PlacesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlacesProvider {

  url = 'http://localhost:8000/api';

  constructor(public http: HttpClient) {
    console.log('Hello PlacesProvider Provider');
  }

  getAllPlaces(): Observable<any> {
    console.log(this.url + '/places');
    return this.http.get(this.url + '/places');
  }

  getAllVisitPlaces(place_id: number): Observable<any> {
    return this.http.get(this.url + '/places/' + place_id.toString() + '/visits');
  }

}
