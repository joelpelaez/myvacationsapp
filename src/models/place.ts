export class Place {
  id: number;
  name: string;
  likes: number;
  img: string;
}

export class VisitPlace {
  id: number;
  place_id: number;
  name: string;
  img: string;
}
