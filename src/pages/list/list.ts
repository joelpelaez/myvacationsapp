import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PlacesProvider} from "../../providers/places/places";

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {

  visitPlaces = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private placesProvider: PlacesProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
    let place_id = this.navParams.get('place_id');
    this.placesProvider.getAllVisitPlaces(place_id).subscribe(value => {
      this.visitPlaces = value;
    }, error => {
      console.log(error);
    })
  }

}
