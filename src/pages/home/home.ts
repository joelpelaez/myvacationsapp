import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListPage } from '../list/list';
import {PlacesProvider} from "../../providers/places/places";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  places = [];

  constructor(public navCtrl: NavController, private placesProvider: PlacesProvider) {

  }

  ionViewDidLoad() {
    this.placesProvider.getAllPlaces().subscribe(value => {
      this.places = value;
    }, error => {
      console.log(error);
    })
  }

  gosub(id: number){
    this.navCtrl.push(ListPage, {place_id: id});
  }

}
